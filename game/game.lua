--example script for gamestate
function gameInitialize()
    value1 = 0
end

function gameUpdate(dt)
    value1 = value1 + dt
end

function gameKeyPressed(key)
    print(key)
end


function gameDraw()
    love.graphics.print("We are in the game state!", 0,0)
    love.graphics.print(value1, 0,15)
end

