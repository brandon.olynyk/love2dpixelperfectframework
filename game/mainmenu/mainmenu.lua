--example script for menu gamestate
function menuInitialize()
    value2 = 0
end

function menuUpdate(dt)
    value2 = value2 + dt
end

function menuKeyPressed(key)
    print(key)
end


function menuDraw()
    love.graphics.print("We are in the menu state!", 0,0)
    love.graphics.print(value2, 0,15)
end

