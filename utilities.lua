--BONUS SCRIPT Utilities for functions such as:
--distance to objects with x and y values
--degrees to radians
--radians to degres

function getDistanceBetween(object1, object2)

	return math.sqrt(math.pow(object1.x - object2.x,2) + math.pow(object1.y - object2.y, 2))
end

function degToRad(angle)
    return angle * (math.pi / 180)
end
    
function radToDeg(angle)
    return angle * ( 180/ math.pi)
end
    
