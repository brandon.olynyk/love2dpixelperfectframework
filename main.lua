function love.load()

	--init video settings
	love.graphics.setDefaultFilter("nearest","nearest",1)

	--uncomment this to set a font
	--font = love.graphics.newFont("prstart.ttf",8)
	--love.graphics.setFont(font)
	
	resolution = {
		--base pixel resolution
		x = 256,
		y = 192
	}

	camera = {
		--camera init with x and y coords
		x = 0,
		y = 0
	}
	
	switchScale(1)
	
	--import game and menu scripts
	require("game/game")
	require("game/mainmenu/mainmenu")
	

	gameState = "mainmenu"
	switchGameState(gameState, true)
	
end

function love.update(dt)
	if gameState == "game" then
		gameUpdate(dt)
	elseif gameState == "mainmenu" then
		menuUpdate(dt)
	end
	
end


function love.keypressed(key)
	if gameState == "game" then
		gameKeyPressed(key)
	elseif gameState == "mainmenu" then
		menuKeyPressed(key)
	end

	
	--global keys that work regardless of game state

	--resolution change with fn keys
	if(key == "f1") then
		switchScale(1)
	end

	if(key == "f2") then
		switchScale(2)
	end

	if(key == "f3") then
		switchScale(3)
	end
	
	--switch gamestate with q and w, switch and reset state with a and s (example)
	if(key == "q") then
	switchGameState("mainmenu", false)
	end
	
	if(key == "w") then
	switchGameState("game", false)
	end
	
	if(key == "a") then
	switchGameState("mainmenu", true)
	end
	
	if(key == "s") then
	switchGameState("game", true)
	end

end	

function love.draw()
	--set graphics scale and camera position
	love.graphics.scale(windowScale,windowScale)
	love.graphics.translate(math.floor(-camera.x),math.floor(-camera.y))

	--prints game here
	if gameState == "game" then
		gameDraw()	
	elseif gameState == "mainmenu" then
		menuDraw()
	end

	--love.graphics.print(love.timer.getFPS(),camera.x, camera.y) --print fps	
end


function switchScale(value)
	--function to switch scale
	--value is screen multiplier (for resolution declared above)
	windowScale = value
	love.window.setMode( resolution.x * windowScale, resolution.y * windowScale, {resizable=true})
end

function switchGameState(state, reset)

	--switch gamestate based on "state" string
	--reset is a bool that dictates if the state's start function should be run
	
	if state == "mainmenu" then
	--start mainmenu
		if reset then
			menuInitialize()
		else
		
		end
		gameState="mainmenu"
	elseif state == "game" then
	--start game
		if reset then
			gameInitialize()
		else
		
		end
		gameState="game"
	end
end

